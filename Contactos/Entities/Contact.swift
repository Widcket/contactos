//
//  Contact.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 29/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//

import Foundation

struct Contact: Codable {
    var id: Int
    var birthDate: Date?
    var firstName: String
    var lastName: String
    var phones: [Phone]?
    var addresses: [Address]?
    var thumbnail: URL?
    var photo: URL?
    var createdAt: Date?
    
    var fullName: String { return "\(firstName) \(lastName)" }
    
    private enum CodingKeys: String, CodingKey {
        case id = "user_id"
        case birthDate = "birth_date"
        case firstName = "first_name"
        case lastName = "last_name"
        case phones
        case addresses
        case thumbnail = "thumb"
        case photo
        case createdAt = "created_at"
    }
}

extension Contact {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let decodedId = try container.decode(String.self, forKey: .id)
        let decodedBirthDate = try container.decode(String.self, forKey: .birthDate)
        let decodedThumbnail = try container.decode(String.self, forKey: .thumbnail)
        let decodedPhoto = try container.decode(String.self, forKey: .photo)
        let decodedCreatedAt = try container.decode(String.self, forKey: .createdAt)
    
        id = Int(decodedId)!
        firstName = try container.decode(String.self, forKey: .firstName)
        lastName = try container.decode(String.self, forKey: .lastName)
        phones = try? container.decode([Phone].self, forKey: .phones)
        addresses = try? container.decode([Address].self, forKey: .addresses)
        thumbnail = URL(string: decodedThumbnail)
        photo = URL(string: decodedPhoto)
        birthDate = DateHelpers.parseAmericanDate(from: decodedBirthDate)
        createdAt = DateHelpers.parseIsoDate(from: decodedCreatedAt)
    }
}
