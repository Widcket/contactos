//
//  Address.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 29/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//

import Foundation

struct Address: Codable {
    var home: String?
    var work: String?
}
