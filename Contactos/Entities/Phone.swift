//
//  Phone.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 29/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//

import Foundation

struct Phone: Codable {
    var type: PhoneType
    var number: String?
    
    private enum CodingKeys: String, CodingKey {
        case type
        case number
    }
}

enum PhoneType: String, Codable {
    case home = "Home"
    case cellphone = "Cellphone"
    case office = "Office"
}
