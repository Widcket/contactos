//
//  ContactListPresenter.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 29/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//

import Foundation

protocol ContactListEventsHandler: class {
    func viewDidLoad()
    func didSelectContact(_ contact: Contact)
}

class ContactListPresenter: ContactListEventsHandler {
    private weak var view: ContactListView?
    private let router: ContactListRouter?
    private let contactsFetcher: FetchContacts
    
    init(router: ContactListRouter, view: ContactListView, contactsFetcher: FetchContacts) {
        self.router = router
        self.view = view
        self.contactsFetcher = contactsFetcher
    }
    
    func fetchContacts() {
        contactsFetcher.execute()
    }
    
    func viewDidLoad() {
        fetchContacts()
    }
    
    func didSelectContact(_ contact: Contact) {
        guard let view = view else { return }
        
        router?.presentContactDetails(from: view, forContact: contact)
    }
}

extension ContactListPresenter: FetchContactsOutput {
    func didFetchContacts(_ contacts: [Contact]) {
        view?.displayContacts(contacts)
    }
    
    func didFailFetchingContacts(error: Error) {
        debugPrint(error)
    }
}
