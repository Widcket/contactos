//
//  ContactListViewController.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 29/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//
//  Default user icon from https://openclipart.org/detail/247319/abstract-user-icon-3 (public domain)
//

import Foundation
import UIKit
import Kingfisher

protocol ContactListView: class {
    func displayContacts(_ contacts: [Contact])
}

class ContactListViewController: UIViewController, ContactListView {
    @IBOutlet weak var contactsTable: UITableView!
    
    var eventsHandler: ContactListEventsHandler?
    
    fileprivate let cellReuseIdentifier: String = "ContactListCell"
    fileprivate var searchController: UISearchController!
    fileprivate var sectionTitles: [String] = []
    fileprivate var allContacts: [Contact] = []
    fileprivate var contactsDictionary: [String: [Contact]] = [:]
    
    fileprivate var filteredContacts: [Contact] = [] {
        didSet { reloadData() }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        setupTableView()
        
        eventsHandler?.viewDidLoad()
    }
    
    func setupTableView() {
        contactsTable.dataSource = self
        contactsTable.delegate = self
        contactsTable.tableFooterView = UIView(frame: .zero)
        contactsTable.register(ContactListCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        contactsTable.rowHeight = 50
        
        if #available(iOS 11.0, *) {
            contactsTable.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    private func setupNavigationBar() {
        searchController = UISearchController(searchResultsController:  nil)
        searchController.searchResultsUpdater = self
        searchController.delegate = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self

        navigationItem.titleView = searchController.searchBar
        navigationItem.hidesBackButton = true
    }
    
    func generateKeys() {
        contactsDictionary = [:]
        
        for contact in filteredContacts {
            let key = String(contact.fullName.prefix(1))
            
            if var contactValues = contactsDictionary[key] {
                contactValues.append(contact)
                
                contactsDictionary[key] = contactValues
            } else {
                contactsDictionary[key] = [contact]
            }
        }
        
        sectionTitles = [String](contactsDictionary.keys)
        sectionTitles = sectionTitles.sorted { previous, next -> Bool in previous < next }
    }
    
    func reloadData() {
        generateKeys()
        contactsTable.reloadData()
    }
    
    // MARK: - View
    
    func displayContacts(_ contacts: [Contact]) {
        allContacts = contacts
        filteredContacts = contacts
    }
}

extension ContactListViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let key = sectionTitles[section]
        
        if let contactValues = contactsDictionary[key] {
            return contactValues.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
        let key = sectionTitles[indexPath.section]
        
        cell.accessoryType = .disclosureIndicator
        cell.imageView?.kf.indicatorType = .activity

        if let contactValues = contactsDictionary[key] {
            let contact = contactValues[indexPath.row]
            
            if let query = searchController.searchBar.text, !query.trim().isEmpty {
                cell.textLabel?.attributedText = contact.fullName.highlight(query, fontSize: 17)
            }
            else {
                cell.textLabel?.attributedText = nil
                cell.textLabel?.text = contact.fullName
            }
            
            cell.imageView?.kf.setImage(with: contact.thumbnail, placeholder: #imageLiteral(resourceName: "defaultAvatar")) { _, _, _, _ in
                cell.setNeedsLayout()
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return sectionTitles
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let key = sectionTitles[indexPath.section]
        
        searchController.searchBar.resignFirstResponder()
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let contactValues = contactsDictionary[key] {
            eventsHandler?.didSelectContact(contactValues[indexPath.row])
        }
    }
}

extension ContactListViewController: UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) { }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        filteredContacts = allContacts
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let searchText = searchController.searchBar.text?.lowercased() else { return }
        
        if searchText.isEmpty {
            filteredContacts = allContacts
        }
        else {
            filteredContacts = allContacts.filter { contact in contact.fullName.lowercased().contains(searchText) }
        }
    }
}
