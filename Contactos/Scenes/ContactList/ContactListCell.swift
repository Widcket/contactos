//
//  ContactListCell.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 29/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//

import Foundation
import UIKit

class ContactListCell: UITableViewCell {
    private let imageViewPadding: CGFloat = 0.2
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Reduce the imageView size
        layoutImageView()
         // Adjust the textLabel margin and width in proportion to the imageView size reduction
        layoutTextLabel()
        // And adjust the separator inset to match
        setSeparatorInset()
    }
    
    private func layoutImageView() {
        guard let imageView = imageView else { return }
        
        let originY = imageView.frame.height * (imageViewPadding / 2) // centers vertically the imageView
        let originX = imageView.frame.origin.x
        let width = imageView.frame.width * (1 - imageViewPadding)
        let height = imageView.frame.height * (1 - imageViewPadding)
        
        imageView.frame = CGRect(x: originX, y: originY, width: width, height: height)
    }
    
    private func layoutTextLabel() {
        guard let textLabel = textLabel else { return }
        
        let originY = textLabel.frame.origin.y
        let originX = textLabel.frame.origin.x / (1 + (imageViewPadding / 2))
        let width = textLabel.frame.width * (1 + (imageViewPadding / 2))
        let height = textLabel.frame.height
        
        textLabel.frame = CGRect(x: originX, y: originY, width: width, height: height)
    }
    
    private func setSeparatorInset() {
        guard let textLabel = textLabel else { return }
        
        let top = separatorInset.top
        let left = textLabel.frame.origin.x
        let bottom = separatorInset.bottom
        let right = separatorInset.right
        
        separatorInset = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
    }
}
