//
//  ContactListRouter.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 29/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//

import Foundation
import UIKit
import Moya

class ContactListRouter {
    class func createModule() -> UIViewController {
        let view = ContactListViewController(nibName: "ContactListView", bundle: Bundle.main)
        let contactsRepository = ContactsRepository(api: MoyaProvider<ContactsApi>())
        let contactsFetcher = FetchContacts(contactsRepository: contactsRepository)
        let presenter = ContactListPresenter(router: ContactListRouter(), view: view, contactsFetcher: contactsFetcher)
        let navigationController = UINavigationController(rootViewController: view)
        
        view.eventsHandler = presenter
        contactsFetcher.output = presenter
        
        return navigationController
    }
    
    func presentContactDetails(from sourceView: ContactListView, forContact contact: Contact) {
        let contactDetailViewController = ContactDetailRouter.createModule(forContact: contact)
        
        if let sourceView = sourceView as? UIViewController {
            sourceView.navigationController?.pushViewController(contactDetailViewController, animated: true)
        }
    }
}
