//
//  ContactDetailPresenter.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 29/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//

import Foundation

protocol ContactDetailEventsHandler: class {
    func viewDidLoad()
}

class ContactDetailPresenter: ContactDetailEventsHandler {
    private weak var view: ContactDetailView?
    private let router: ContactDetailRouter?
    private let contactFetcher: FetchContact
    private var contact: Contact
    
    init(router: ContactDetailRouter, view: ContactDetailView, contactFetcher: FetchContact, contact: Contact) {
        self.router = router
        self.view = view
        self.contactFetcher = contactFetcher
        self.contact = contact
    }
    
    func fetchContact(id: Int) {
        contactFetcher.execute(id: id)
    }
    
    func viewDidLoad() {
        fetchContact(id: contact.id)
        view?.displayContact(getContactToDisplay())
    }
    
    func getContactToDisplay() -> ContactToDisplay {
        let fullName = contact.fullName
        let photo = contact.photo
        var filteredPhones: [Phone] = []
        var filteredAddresses: [AddressToDisplay] = []
        
        if let phones = contact.phones {
            filteredPhones = filterPhones(phones)
        }
        
        if let addresses = contact.addresses {
            filteredAddresses = filterHomeAddresses(addresses) + filterWorkAddresses(addresses)
        }
        
        return ContactToDisplay(fullName: fullName, phones: filteredPhones, addresses: filteredAddresses, photo: photo)
    }
    
    func filterPhones(_ phones: [Phone]) -> [Phone] {
        return phones.filter { phone -> Bool in phone.number != nil }
    }
    
    func filterHomeAddresses(_ addresses: [Address]) -> [AddressToDisplay] {
        let filteredAddresses = addresses.filter { address -> Bool in address.home != nil }
        
        return filteredAddresses.compactMap{ address -> AddressToDisplay in AddressToDisplay(type: .home, value: address.home!) }
    }
    
    func filterWorkAddresses(_ addresses: [Address]) -> [AddressToDisplay] {
        let filteredAddresses = addresses.filter { address -> Bool in address.work != nil }
        
        return filteredAddresses.compactMap{ address -> AddressToDisplay in AddressToDisplay(type: .work, value: address.work!) }
    }
}

extension ContactDetailPresenter: FetchContactOutput {
    func didFetchContact(_ contact: Contact) {
        self.contact = contact
        
        view?.displayContact(getContactToDisplay())
    }
    
    func didFailFetchingContact(error: Error) {
        debugPrint(error)
    }
}
