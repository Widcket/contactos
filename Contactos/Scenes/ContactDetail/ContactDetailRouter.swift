//
//  ContactDetailRouter.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 29/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//

import Foundation
import UIKit
import Moya

class ContactDetailRouter {
    class func createModule(forContact contact: Contact) -> UIViewController {
        let view = ContactDetailViewController(nibName: "ContactDetailView", bundle: Bundle.main)
        let contactsRepository = ContactsRepository(api: MoyaProvider<ContactsApi>())
        let contactFetcher = FetchContact(contactsRepository: contactsRepository)
        let presenter = ContactDetailPresenter(router: ContactDetailRouter(), view: view, contactFetcher: contactFetcher, contact: contact)
        
        view.eventsHandler = presenter
        contactFetcher.output = presenter
        
        return view
    }
}
