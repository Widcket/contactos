//
//  ContactDetailViewController.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 29/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//
//  Default user icon from https://openclipart.org/detail/247319/abstract-user-icon-3 (public domain)
//

import UIKit

protocol ContactDetailView: class {
    func displayContact(_ contact: ContactToDisplay)
}

class ContactDetailViewController: UIViewController, ContactDetailView {
    @IBOutlet weak var contactPhoto: UIImageView!
    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var detailsTable: UITableView!
    
    var eventsHandler: ContactDetailEventsHandler?
    
    var detailItem: ContactToDisplay? {
        didSet { updateDetails() }
    }
    
    fileprivate var phonesSection = -1
    fileprivate var addressesSection = -1
    fileprivate let cellReuseIdentifier: String = "DefaultCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        eventsHandler?.viewDidLoad()
    }
    
    func setupTableView() {
        detailsTable.dataSource = self
        detailsTable.delegate = self
        detailsTable.alwaysBounceVertical = false
        detailsTable.tableFooterView = UIView(frame: .zero)
    }
    
    func updateDetails() {
        contactPhoto.kf.indicatorType = .activity
        contactPhoto.kf.setImage(with: detailItem?.photo, placeholder: #imageLiteral(resourceName: "defaultPhoto"))
        contactName.text = detailItem?.fullName
        detailsTable.reloadData()
    }
    
    // MARK: - View
    
    func displayContact(_ contact: ContactToDisplay) {
        detailItem = contact
    }
}

extension ContactDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        var currentSection = 0
        
        guard let detailItem = detailItem else { return currentSection }
        
        if !detailItem.phones.isEmpty {
            phonesSection = currentSection
            currentSection += 1
        }
        
        if !detailItem.addresses.isEmpty {
            addressesSection = currentSection
            currentSection += 1
        }
        
        return currentSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let detailItem = detailItem else { return 0 }
        
        return section == phonesSection ? detailItem.phones.count : detailItem.addresses.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == phonesSection { return "Teléfonos" }
        if section == addressesSection { return "Direcciones" }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // dequeueReusableCell:withIdentifier: does not initialize the cell by default
        // dequeueReusableCell(withIdentifier:forIndexPath: does
        // The initialization only happens when the cell is not yet in the reuse stack
        // and we actually want to initialize it manually because of the .value2 style
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) ??
            UITableViewCell(style: .value2, reuseIdentifier: cellReuseIdentifier)
        
        guard let detailItem = detailItem else { return cell }
        
        if indexPath.section == phonesSection {
            setPhoneDetails(cell: cell, row: indexPath.row, forContact: detailItem)
        }
        else {
            setAddressDetails(cell: cell, row: indexPath.row, forContact: detailItem)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func setPhoneDetails(cell: UITableViewCell, row: Int, forContact contact: ContactToDisplay) {
        switch contact.phones[row].type {
        case .home:
            cell.textLabel?.text = "Casa"
        case .cellphone:
            cell.textLabel?.text = "Celular"
        case .office:
            cell.textLabel?.text = "Oficina"
        }
        
        cell.detailTextLabel?.text = contact.phones[row].number
    }
    
    func setAddressDetails(cell: UITableViewCell, row: Int, forContact contact: ContactToDisplay) {
        cell.detailTextLabel?.text = contact.addresses[row].value
        cell.textLabel?.text = contact.addresses[row].type.rawValue
    }
}
