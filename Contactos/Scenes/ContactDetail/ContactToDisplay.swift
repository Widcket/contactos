//
//  ContactToDisplay.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 30/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//

import Foundation

struct ContactToDisplay {
    var fullName: String
    var phones: [Phone]
    var addresses: [AddressToDisplay]
    var photo: URL?
}

struct AddressToDisplay {
    var type: AddressTypeToDisplay
    var value: String
}

enum AddressTypeToDisplay: String {
    case home = "Casa"
    case work = "Trabajo"
}
