//
//  ContactsRepository.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 29/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//

import Foundation
import Moya
import RxSwift

class ContactsRepository {
    private let api: MoyaProvider<ContactsApi>
    
    init(api: MoyaProvider<ContactsApi>) {
        self.api = api
    }
    
    public func getContacts() -> Observable<[Contact]> {
        return api.rx
            .request(ContactsApi.contactList)
            .asObservable()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .map([Contact].self, atKeyPath: nil, using: JSONDecoder())
    }
    
    public func getContact(id: Int) -> Observable<Contact> {
        return api.rx
            .request(ContactsApi.contactDetail(id: id))
            .asObservable()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .map(Contact.self, atKeyPath: nil, using: JSONDecoder())
    }
}
