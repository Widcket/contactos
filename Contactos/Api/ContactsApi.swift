//
//  ContactsApi.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 29/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//

import Foundation
import Moya

enum ContactsApi {
    case contactList
    case contactDetail(id: Int)
}

extension ContactsApi: TargetType {
    var baseURL: URL { return URL(string: Config.contactsUrl)! }
    var method: Moya.Method { return .get }
    var headers: [String : String]? { return ["Accept": "application/json"] }
    var parameterEncoding: ParameterEncoding { return URLEncoding.default }
    
    var path: String {
        switch self {
        case .contactList:
            return "/contacts"
        case .contactDetail(let id):
            return "/contacts/\(id)"
        }
    }
    
    var task: Task {
        switch self {
        case .contactList:
            fallthrough
        case .contactDetail(_):
            return .requestParameters(parameters: [:], encoding: URLEncoding.default)
        }
    }
    
    var sampleData: Data {
        switch self {
        case .contactList:
            return ("[{\"user_id\":\"100\",\"created_at\":\"2015-08-05T08:40:51.620Z\",\"birth_date\":\"2000-01-31\",\"first_name\":\"Susana\"," +
                "\"last_name\":\"Ducatti\",\"phones\":[{\"type\":\"Home\",\"number\":\"54-11-4787-2012\"},{\"type\":\"Cellphone\",\"number\":" +
                "\"54-911-3211-0936\"},{\"type\":\"Office\",\"number\":null}],\"thumb\":\"https://upload.wikimedia.org/wikipedia/commons/thumb/" +
                "c/c9/Creative-Tail-People-women-skintone4.svg/128px-Creative-Tail-People-women-skintone4.svg.png\",\"photo\":\"https://static" +
                ".pexels.com/photos/27411/pexels-photo-27411.jpg\"},{\"user_id\":\"1001\",\"created_at\":\"2014-08-05T08:40:51.620Z\",\"birth_date" +
                "\":\"1995-11-21\",\"first_name\":\"Roberto\",\"last_name\":\"Mancariotti\",\"phones\":[{\"type\":\"Home\",\"number\":null},{\"" +
                "type\":\"Cellphone\",\"number\":\"54-911-2011-1230\"},{\"type\":\"Office\",\"number\":null}],\"thumb\":\"https://upload.wikimedia" +
                ".org/wikipedia/commons/thumb/5/5a/Creative-Tail-People-man.svg/128px-Creative-Tail-People-man.svg.png\",\"photo\":\"https://static" +
                ".pexels.com/photos/91227/pexels-photo-91227.jpeg\"}]").data(using: .utf8)!
        case .contactDetail(_):
            return ("{\"user_id\":\"100\",\"created_at\":\"2015-08-05T08:40:51.620Z\",\"birth_date\":\"2000-01-31\",\"first_name\":\"Susana\"," +
                "\"last_name\":\"Ducatti\",\"phones\":[{\"type\":\"Home\",\"number\":\"54-11-4787-2012\"},{\"type\":\"Cellphone\",\"number\":" +
                "\"54-911-3211-0936\"},{\"type\":\"Office\",\"number\":null}],\"thumb\":\"https://upload.wikimedia.org/wikipedia/commons/thumb/" +
                "c/c9/Creative-Tail-People-women-skintone4.svg/128px-Creative-Tail-People-women-skintone4.svg.png\",\"photo\":\"https://static" +
                ".pexels.com/photos/27411/pexels-photo-27411.jpg\",\"addresses\":[{\"work\":\"Av. Rivadavia 1001 4B\"}]}").data(using: .utf8)!
        }
    }
}
