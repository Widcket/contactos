//
//  DateHelpers.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 30/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//

import Foundation

class DateHelpers {
    class func parseAmericanDate(from string: String) -> Date? {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.isLenient = true
        
        return formatter.date(from: string)
    }
    
    class func parseIsoDate(from string: String) -> Date? {
        let formatter = ISO8601DateFormatter()
        
        formatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
        
        return formatter.date(from: string)
    }
}
