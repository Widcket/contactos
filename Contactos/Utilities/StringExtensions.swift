//
//  StringExtensions.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 30/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func trim() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func highlight(_ text: String, fontSize: CGFloat = UIFont.systemFontSize) -> NSAttributedString {
        let haystack = self.trim().lowercased()
        
        guard let range = haystack.range(of: text.trim().lowercased()) else {
            return bold(range: NSRange(location: 0, length: 0))
        }
        
        let needle = text.trim().lowercased()
        
        if let lower16 = range.lowerBound.samePosition(in: haystack.utf16) {
            let start = haystack.utf16.distance(from: haystack.utf16.startIndex, to: lower16)
            
            return bold(range: NSRange(location: start, length: needle.count), fontSize: fontSize)
        }
        
        return NSAttributedString(string: text)
    }
    
    func bold(range boldRange: NSRange, fontSize: CGFloat = UIFont.systemFontSize) -> NSAttributedString {
        let boldAttributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: fontSize)]
        let nonBoldAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: fontSize)]
        let attributedString = NSMutableAttributedString(string: self, attributes: nonBoldAttributes)
        
        attributedString.setAttributes(boldAttributes, range: boldRange)
        
        return attributedString
    }
}
