//
//  FetchContacts.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 29/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//

import Foundation
import RxSwift

protocol FetchContactsOutput: class {
    func didFetchContacts(_ contacts: [Contact])
    func didFailFetchingContacts(error: Error)
}

class FetchContacts {
    weak var output: FetchContactsOutput?
    private var contactsRepository: ContactsRepository
    private var disposeBag: DisposeBag = DisposeBag()
    
    init(contactsRepository: ContactsRepository) {
        self.contactsRepository = contactsRepository
    }
    
    func execute() {
        contactsRepository.getContacts()
            .subscribe(
                onNext: { [weak self] contacts in
                    self?.output?.didFetchContacts(contacts)
            }, onError: { [weak self] error in
                    self?.output?.didFailFetchingContacts(error: error)
            })
            .disposed(by: disposeBag)
    }
}
