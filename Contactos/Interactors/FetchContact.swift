//
//  FetchContact.swift
//  Contactos
//
//  Created by Rita Zerrizuela on 29/07/2018.
//  Copyright © 2018 Widcket. All rights reserved.
//

import Foundation
import RxSwift

protocol FetchContactOutput: class {
    func didFetchContact(_ contact: Contact)
    func didFailFetchingContact(error: Error)
}

class FetchContact {
    weak var output: FetchContactOutput?
    private var contactsRepository: ContactsRepository
    private var disposeBag: DisposeBag = DisposeBag()
    
    init(contactsRepository: ContactsRepository) {
        self.contactsRepository = contactsRepository
    }
    
    func execute(id: Int) {
        contactsRepository.getContact(id: id)
            .subscribe(
                onNext: { [weak self] contact in
                    self?.output?.didFetchContact(contact)
            }, onError: { [weak self] error in
                    self?.output?.didFailFetchingContact(error: error)
            })
            .disposed(by: disposeBag)
    }
}
